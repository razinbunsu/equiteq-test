console.log('Hello Equiteq');

(function($) {
    $(document).ready(function(){

        // store filter for each group
        var dropdownFilters = {};
        var dropdownFilter;
        // quick search regex
        var qsRegex;

        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
              columnWidth: '.grid-sizer'
            },
            filter: function() {
                var $this = $(this);
                var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
                var filterResult = dropdownFilter ? $this.is( dropdownFilter ) : true;
                return searchResult && filterResult;
            },
        });

        $('.filters').on( 'change', function( event ) {
            var $select = $( event.target );
            // get group key
            var filterGroup = $select.attr('value-group');
            // set filter for group
            dropdownFilters[ filterGroup ] = event.target.value;
            // combine filters
            dropdownFilter = concatValues(dropdownFilters );
            // Isotope arrange
            $grid.isotope();
            // Check if the filter result is empty
            showNoResultMessage();
        });

        // use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
            // Check if the filter result is empty
            showNoResultMessage();
        }) );

        // flatten object by concatting values
        function concatValues( obj ) {
            var value = '';
            for ( var prop in obj ) {
                value += obj[ prop ];
            }
            return value;
        }

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            threshold = threshold || 100;
            return function debounced() {
                clearTimeout( timeout );
                var args = arguments;
                var _this = this;
                function delayed() {
                    fn.apply( _this, args );
                }
                timeout = setTimeout( delayed, threshold );
            };
        }

        // Show or hide the empty message based on visibility of items
        function showNoResultMessage() {
            var $emptyMessage = $('.no-result');
            if ($grid.data('isotope').filteredItems.length === 0) {
                $emptyMessage.removeClass('d-none');
            } else {
                $emptyMessage.addClass('d-none');
            }
        }

    });
})( jQuery );
