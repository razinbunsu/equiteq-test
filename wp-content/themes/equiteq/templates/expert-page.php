<?php
/* Template Name: Expert Page */
get_header();
$experts = get_experts();
$id = get_the_ID();
$page = get_post($id);
$industries = get_industries();
$locations = get_locations();
?>

<?php

/** Hero **/
hm_get_template_part('template-parts/hero', ['page' => $page]);

?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4"><?php echo $page->intro_title ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php echo $page->post_content ?>
            </div>
        </div>
        <!--May implement the search and filter here-->
        <div class="filters">
            <div class="row">
                <div class="col-md-8 col-12">
                    <label class="text-uppercase">Filters</label>
                    <div class="dropdown-filters">
                        <div class="filter-item">
                            <select id="filter-sector" class="selectpicker" title="Sector" data-style="btn-link" value-group="sector">
                                <option value="">All</option>
                                <?php 
                                    foreach ($industries as $industry) : 
                                    $industry_title = sanitize_title($industry->post_title);
                                ?> 
                                <option value=".<?php echo $industry_title; ?>"><?php echo $industry->post_title; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="filter-item">
                            <select id="filter-location" class="selectpicker" title="Location" data-style="btn-link" value-group="location">
                                <option value="">All</option>
                                <?php 
                                    foreach ($locations as $location) : 
                                    $location_title = sanitize_title($location->post_title); 
                                ?> 
                                <option value=".<?php echo $location_title; ?>"><?php echo $location->post_title; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <label class="text-uppercase">Search</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control quicksearch border-right-0">
                        <div class="input-group-append">
                            <button class="btn btn-link border-left-0 rounded-0 py-1" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--May implement the experts profile list here-->
<section>
    <div class="container no-pad-gutters">
        <div class="expert-list">
            <div class="grid row">
                <div class="grid-sizer col-md-3 col-6"></div>
                <?php 
                    foreach ($experts as $expert) : 

                    $profile_image = get_field('profile_image', $expert);
                    $location = get_field('location', $expert);
                    $location_title = sanitize_title($location->post_title);
                    $industry_expertise = get_field('industry_expertise', $expert);

                    $sectors = [];
                    if (!empty($industry_expertise) && is_array($industry_expertise)) {
                        foreach ($industry_expertise as $post_object) {
                            $sectors[] = $post_object->post_title;
                        }
                    }
                ?>
                <div class="grid-item col-md-3 col-6 <?php foreach ($sectors as $sector) : echo sanitize_title($sector) . ' '; endforeach; echo $location_title; ?>">    
                    <div class="grid-item-content expert-cards">
                        <div class="expert-card card">
                            <?php if ($profile_image): ?>
                            <figure class="mx-auto p-1">
                                <a href="<?php echo esc_url(get_permalink($expert)); ?>">
                                    <img class="expert-img" src="<?php echo esc_url($profile_image['url']); ?>" alt="<?php echo esc_attr($profile_image['alt']); ?>" />
                                </a>
                            </figure>
                            <?php endif; ?>
                            <div class="card-body pt-2 text-center">
                                <h3 class="expert-title">
                                    <a href="<?php echo esc_url(get_permalink($expert)); ?>">
                                        <?php echo $expert->post_title; ?>
                                    </a>
                                </h3>   
                                <p class="expert-designation mb-2">
                                    <?php echo get_field( 'title', $expert ); ?>
                                </p>
                                <p class="expert-location font-italic mb-2">
                                    <?php echo $location->post_title; ?>
                                </p>
                                <ul class="experts-socials list-unstyled">
                                    <?php if ( get_field('email', $expert) ): ?>
                                    <li>
                                        <a href="mailto:<?php echo get_field('email', $expert); ?>" target="_blank">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php if ( get_field('contact_no', $expert) ): ?>
                                    <li>
                                        <a href="tel:<?php echo get_field('contact_no', $expert); ?>" target="_blank">
                                            <i class="fa fa-phone"></i>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php if ( get_field('linkedin', $expert) ): ?>
                                    <li>
                                        <a href="<?php echo get_field('linkedin', $expert); ?>" target="_blank">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="no-result d-none">
            <h3 class="text-uppercase">No Results Found</h3>
            <p>Oops! It looks like there are no items matching your search/filter criteria.</p>
        </div>
    </div>
</section>

<?php
get_footer();
?>