<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);
$profile_image = get_field('profile_image');
$city = get_field('location');
$industry_expertise = get_field('industry_expertise');
?>


<section id="post-<?php the_ID(); ?>" <?php post_class('single-expert'); ?>>
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a
                href="<?php echo home_url('team'); ?>" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>
        <!--May implement the expert's profile here -->
        <div class="row">
            <div class="col-md-4 col-12">
                <?php if ($profile_image): ?>
                <figure class="expert-thumbnail">
                    <img class="img-fluid rounded-circle" src="<?php echo esc_url($profile_image['url']); ?>" alt="<?php echo esc_attr($profile_image['alt']); ?>" />
                </figure>
                <?php endif; ?>
            </div>
            <div class="col-md-8 col-12">

                <h1 class="text-uppercase">
                    <?php the_title(); ?>
                </h1>

                <?php if ( get_field('title') ): ?>
                <h6 class="expert-designation pt-2 text-uppercase">
                    <?php echo get_field('title'); ?>
                </h6>
                <?php endif; ?>

                <?php if ( $city ): ?>
                <div class="expert-location pt-4">
                    <p class="text-uppercase">
                        <i class="fa fa-map-marker fa-lg text-green" aria-hidden="true"></i>
                        <?php echo $city->post_title; ?>
                    </p>
                </div>
                <?php endif; ?>

                <ul class="experts-socials list-unstyled pt-2">
                    <?php if ( get_field('email') ): ?>
                    <li>
                        <a href="mailto:<?php echo get_field('email'); ?>" target="_blank">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if ( get_field('contact_no') ): ?>
                    <li>
                        <a href="<?php echo get_field('contact_no'); ?>" target="_blank">
                            <i class="fa fa-phone"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if ( get_field('linkedin') ): ?>
                    <li>
                        <a href="<?php echo get_field('linkedin'); ?>" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>

                <div class="expert-description">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!--May implement the expert's industry expertise here -->
<?php if ($industry_expertise): ?>
<section class="mb-5 bg-dark-blue">
    <div class="container no-pad-gutters">
        <h6 class="icon-title text-white text-uppercase mb-5">Expertise</h6>
        <div class="row">
            <?php 
                $count = 0;
                foreach ($industry_expertise as $expertise) : 
                $icon = get_field('icon', $expertise);
                $count++;
            ?>
                <div class="col-12 col-md-4">
                    <div class="p-2 <?php echo ($count <= 3) ? 'mb-2 mb-md-5' : 'mb-2 mb-md-0'; ?>">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <img class="img-fluid" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
                            </div>
                            <div class="col-9">
                            <p class="text-white"><?php echo $expertise->post_title; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<?php
get_footer();